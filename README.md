# Typist

A simple, easy to style live-typer, inspired by [typr](https://github.com/mog/typr).

Works best with [Firefox](https://www.mozilla.org/en-US/firefox/new/).
